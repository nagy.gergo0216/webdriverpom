﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;
using SearchWidget = WebdriverClass.WidgetsAtClass.SearchWidget;
using ResultWidget = WebdriverClass.WidgetsAtClass.ResultWidget;

namespace WebdriverClass
{
    class PageObjectTestAtClass : TestBase
    {
        [Test]
        public void PageObjectSearchExample()
        {
            // TASK 1.1: implement a static navigate function to search page which returns a search page instance
            var pageToTest = SearchPage.Navigate(Driver, "http://elvira.mav-start.hu/elvira.dll/x/index?language=1");
            // TASK 1.2: implement GetSearchWidget function and instantiate searchWidget 
            SearchWidget searchWidget = pageToTest.GetSearchWidget();
            // TASK 2.1: search for route from "Budapest" to "Szeged" via "Kecskemet" using searchWidget
            searchWidget.SetRoute("Budapest", "Szeged", "Kecskemét");
            // TASK 2.2: set reduction to "Tanuló bérlet" using searchWidget
            searchWidget.SetReduction(SearchWidget.Reductions.TanuloBerlet);
            // TASK 2.3: set search option to SearchWidget.searchOptions.PotjegyNelkul
            searchWidget.SetSearchOptionTo(SearchWidget.SearchOptions.PotjegyNelkul);
            // TASK 3.1: implement searchWidget's ClickTimetableButton function to click on Menetrend button and return a search page instance
            searchWidget.ClickTimetableButton();
            // TASK 3.2: implement GetResultWidget function and instantiate resultWidget 
            ResultWidget resultWidget = pageToTest.GetResultWidget();
            
            ;
            // TASK 4.1: Finish ResultWidget to give back the number of results
            Assert.Greater(resultWidget?.GetNoOfResults(), 0);
        }
    }
}