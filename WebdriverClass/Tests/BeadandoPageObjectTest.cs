﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.Tests
{
    
    class BeadandoPageObjectTest :TestBase
    {

        static IEnumerable testData()
        {
            return new string[] { "1", "2" };
        }

        [Test, TestCaseSource("testData")]
        public void SurveyTest(string radioButtonIndex)
        {
            var pageToTest = SearchPage.Navigate(Driver, "https://www.surveymonkey.com/r/FNRVK86");
            SurveySearchWidget searchWidget = pageToTest.GetSurveySearchWidget();
            //firstTask
            searchWidget.SetRadioButton(radioButtonIndex);
            // secondTask
            searchWidget.SetRadioButton("7");
            //thirdTask
            searchWidget.SetComboBox("Page Object");
            //fourthTask
            searchWidget.SetRatingContainer();
            searchWidget.PressButtonOnFirstPage();
            
            //fifthTask
            searchWidget.SetRadioButtonOnSecondPage("3");
            searchWidget.SetMessageBox("Nagyon gyakoriak a beadandók, de egyébként jó élmény volt.");
            searchWidget.PressButtonOnSecondPage();
            var title = searchWidget.Title;
            Assert.IsTrue(title.Contains("Thank you! Create Your Own Online Survey Now!"));
        }
    }
}
