﻿using System.Runtime.InteropServices;
using OpenQA.Selenium;
using WebdriverClass.WidgetsAtClass;
using SearchWidget = WebdriverClass.WidgetsAtClass.SearchWidget;

namespace WebdriverClass.PagesAtClass
{
    class SearchPage : BasePage
    {
      
        public SearchPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        // TASK 1.1: implement a static navigate function to search page which returns a search page instance
        /*public static SearchPage navigate(IWebDriver webDriver)
        {
            // Navigate to "http://elvira.mav-start.hu/elvira.dll/x/index?language=1"
            // Return new SearchPage instance
        }*/
        public static SearchPage Navigate(IWebDriver webDriver,string url)
        {
            webDriver.Navigate().GoToUrl(url);
            return new SearchPage(webDriver);
        }

        // TASK 1.2: implement getSearchWidget function
        public SearchWidget GetSearchWidget()
        {
            return new SearchWidget(Driver);
        }

        public SurveySearchWidget GetSurveySearchWidget()
        {
            return new SurveySearchWidget(Driver);
        }

        // TASK 3.2: implement getResultWidget function and instantiate resultWidget
        public ResultWidget GetResultWidget()
        {
            return new ResultWidget(Driver);
        }
    }
}
