﻿using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using WebdriverClass.PagesAtClass;
using FindsByAttribute=SeleniumExtras.PageObjects.FindsByAttribute;
using How = SeleniumExtras.PageObjects.How;

namespace WebdriverClass.WidgetsAtClass
{
    class ResultWidget : BasePage
    {
        public ResultWidget(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver,this);
        }

        
        // TASK 4.2: Create a new timetable webelement using FindsBy annotation
        [FindsBy(How = How.Id, Using = "timetable")]
        private IWebElement Timetable { get; set; }

        public int GetNoOfResults()
        {

           return Timetable.FindElements(By.TagName("tr")).Count;
           
        }
    }
}
